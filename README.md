## Captures d'écran

![Image 000](screenshoot/image000.png)

## FR :fr:

Ce projet personnel est un site web dit responsive, c'est-à-dire adaptable sur tout support numérique (Ordinateur, Mobile, Tablette,  etc).

Il s'intitule **Resto Yolo** et utilise les technologies suivantes :

* **HTML** *(Version 5)*
* **CSS**  *(Version 3)*

#### Installation

Vous pouvez visualiser le site web via l'URL suivante : https://suijoart.gitlab.io/resto-yolo/html/home.html
